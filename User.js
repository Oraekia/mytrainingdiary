class User {
    
    constructor(id, username, password, date_of_birth, city) {
        this.id = id
        this.username = username
        this.password = password
        this.date_of_birth = date_of_birth
        this.city = city
    }
}

module.exports = User;