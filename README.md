# MyTD

# Prerequisites

Install node.js and npm

Go to the project directory:
```bash
cd mytrainingdiary/
```


# Install dependencies
```bash
npm install
```


# Launch the server
```bash
node .
```