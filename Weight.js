class Weight {
    
    constructor(id, value, date, user_id) {
        this.id = id
        this.value = value
        this.date = date
        this.user_id = user_id
    }
}

module.exports = Weight;