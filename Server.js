const express = require('express')
const expressHandlebars = require('express-handlebars')
const myDatabase = require('./Database')
const auth = require("./EnsureAuth")
const bodyParser = require('body-parser')
const session = require('express-session')
const bearerToken = require('express-bearer-token');
const jwt = require('jsonwebtoken');

const app = express()

let myDB = new myDatabase()

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({secret:"testSecret", resave:false, saveUninitialized:true}))
app.use(function(request, response, next) {
    response.locals.account = request.session.account
    next()
})
app.use(bearerToken());

app.engine("hbs", expressHandlebars({
    defaultLayout:"defaultLayout.hbs"
}))

app.get("/", function (request, response) {
    if (!request.session.account)
    {
        response.render('Home.hbs') 
    }
    else {
        response.redirect("/User/" + request.session.account.id)
    }
})

//#region (API) Basic routes (Login, Logout, Contact, About, Register)

    //#region Basic routes
    app.get("/Login", function (request, response) {
        response.render("Login.hbs")
    })

    app.post("/Login", function (request, response) {
        var username = request.body.username
        var password = request.body.password

        myDB.CheckPasswordFromUser(username, password, function (status) {
            if (status)
            {
                myDB.SelectUser(username, function (user){
                    request.session.account = user
                    response.redirect("/User/" + user.id)
                })
            }
            else
            {
                const model = {
                    errors: "Username or password incorrect, please retry."
                }
                response.render("Login.hbs", model)
            }
        })
    })

    app.get("/About", function (request, response) {
        response.render("About.hbs")
    })
    
    app.get("/Register", function (request, response) {
        response.render("Register.hbs")
    })
    
    app.post("/Register", function (request, response) {
        myDB.CreateUser(request.body.username, request.body.password, request.body.date_of_birth, request.body.city, function (status) {
            if (status) {
                response.redirect("/login")
            }
            else {
    
                const model = {
                    message: "Username already exist.",
                    date: request.body.date_of_birth,
                    city: request.body.city
                }
    
                response.render("Register.hbs", model)
            }
        })
    })
    
    app.get("/Accounts", function(request, response) {
        myDB.SelectUsers(function(users) {
            const model = {
                accounts: users
            }
            response.render("Accounts.hbs", model)
        })
    })

    app.get("/Account/:id", function(request, response) {
        myDB.SelectUserById(request.params.id, function(user) {
            myDB.SelectWeights(request.params.id, function(weights){
                myDB.SelectActivities(request.params.id, function(activities) {
                    model = {
                        user: user,
                        weight: weights[weights.length - 1],
                        activities: activities
                    }
                    response.render("Account.hbs", model)
                })
            })
    })
})

    app.get("/Contact", function (request, response) {
        response.render("Contact.hbs")
    })


    app.get("/SignOut", function(request, response) {
        request.session.account = null
        response.redirect("/")
    })

    //#endregion

    //#region API basic routes

        app.post("/api/tokens", function(request, response) {
                var username = request.body.username
                var password = request.body.password
                var grant_type = request.body.grant_type

                if (grant_type == "password") {
                    myDB.CheckPasswordFromUser(username, password, function (status) {
                        if (status)
                        {
                            myDB.SelectUser(username, function (user){

                                const IDToken = jwt.sign({  
                                    sub: user.id,
                                    preferred_username: user.username
                                }, 'secretkey', { expiresIn: '1d' });
        
                                const AccessToken = jwt.sign({
                                    userId: user.id
                                }, 'secretaccesskey', { expiresIn: '1h' });
        
                                return response.send({
                                    access_token: AccessToken,
                                    token_type: "Bearer",
                                    id_token: IDToken
                                });
                            })
                        }
                        else
                        {
                            response.status(400)
                            response.json({
                                error: "invalid_grant"
                            })
                        }
                })
            }
            else {
                response.status(400)    
                response.json({
                error: "invalid_grant_type"
                })
            }
        })

    //#endregion

//#endregion

//#region User Requests
    //#region Basics
            app.get("/User/:id", function (request, response) {
                if (request.session.account == null || request.session.account.id != request.params.id) {
                    return response.status(401).send()
                } else {
                    myDB.SelectWeights(request.session.account.id, function(weights){

                        myDB.SelectActivities(request.session.account.id, function(activities) {
                            model = {
                                account: request.session.account,
                                weight: weights[weights.length - 1],
                                activities: activities
                            }
                            response.render("User.hbs", model)
                        })
                    })
                }
            })

            app.get("/User/:id/ModifyUser", function(request, response) {
                if (request.session.account == null || request.session.account.id != request.params.id) {
                    return response.status(401).send()
                } else {
                    const model = {
                        user: request.session.account
                    }
                    response.render("ModifyUser.hbs", model)
                }
            })

            app.post("/User/:id/ModifyUser", function(request, response) {
                if (request.body.password == "") {
                    myDB.ModifyUserWithoutPassword(request.session.account.id, request.body.username, request.body.date_of_birth, request.body.city, function (status) {
                        if (status) {
                            myDB.SelectUser(request.body.username, function (user) {
                            request.session.account = user
                            response.redirect("/")
                            })
                        }
                        else {
                            const model = {
                                message: "Username already exist.",
                                date: request.body.date_of_birth,
                                city: request.body.city
                            }
                
                            response.render("ModifyUser.hbs", model)
                        }
                    })
                } else {
                    myDB.ModifyUser(request.session.account.id, request.body.username, request.body.password, request.body.date_of_birth, request.body.city, function (status) {
                        if (status) {
                            myDB.SelectUser(request.body.username, function (user) {
                                request.session.account = user
                                response.redirect("/")
                            })
                        }
                        else {
                            const model = {
                                message: "Username already exist.",
                                date: request.body.date_of_birth,
                                city: request.body.city
                            }
                
                            response.render("ModifyUser.hbs", model)
                        }
                    })
    
                }
            })


            app.get("/User/:id/NewWeight", function(request, response){
                if (request.session.account == null || !request.session.account.id)
                {
                    return response.status(401).send()
                } else {
                    response.render("NewWeight.hbs")
                }
            })

            app.post("/User/:id/NewWeight", function(request, response){
                if (request.body.value < 20) {
                    const model = {
                        error : "You can't add a weight inferior of 20 kg"
                    }
                    response.render("NewWeight.hbs", model)
                } else {
                    myDB.CreateWeight(request.session.account.id, request.body.value, request.body.date, function (status) {
                        if (status) {
                            response.redirect("/")
                        } else {
                            const model = {
                                error: "There is a problem with the server, Try again."
                            }
                            response.render("NewWeight.hbs", model)
                        }
                    })
                }
            })

            app.get("/User/:id/Weights", function(request, response) {
                myDB.SelectWeights(request.session.account.id, function(weights) {
                    const model = {
                        weights: weights
                    }
                    response.render("Weights.hbs", model)
                })
            })

            app.get("/User/:idWeight/DeleteWeight", function(request, response) {
                myDB.DeleteWeight(request.params.idWeight, function() {
                    response.redirect("/User/" + request.session.account.id + "/Weights")
                })
            })

            app.get("/User/:id/NewActivity", function(request, response){
                if (request.session.account == null || !request.session.account.id)
                {
                    return response.status(401).send()
                } else {
                    response.render("NewActivity.hbs")
                }
            })

            app.post("/User/:id/NewActivity", function(request, response) {

                if (request.body.starttime > request.body.endtime)
                {
                    const model = {
                        error: "Your activity ended before it started."
                    }
                    response.render("NewActivity.hbs", model)
                } else {
                    myDB.CreateActivity(request.session.account.id, request.body.name, request.body.starttime + " " + request.body.starthour, request.body.endtime + " " + request.body.endhour, request.body.description, function (status) {
                        if (status) {
                            response.redirect("/")
                        } else {
                            const model = {
                                error: "There is a problem with the server, Try again."
                            }
                            response.render("NewActivity.hbs", model)
                        }
                    })
                }
            })

            app.get("/User/:idActivity/DeleteActivity", function(request, response){
                    myDB.DeleteActivity(request.params.idActivity, function() {
                        response.redirect("/")
                    })
            })

            app.get("/User/:idActivity/ModifyActivity", function(request, response){
                if (request.session.account == null || !request.session.account.id)
                {
                    return response.status(401).send()
                } else {
                    myDB.SelectActivity(request.params.idActivity, function(activity) {
                        const model = {
                            activity: activity
                        }
                        response.render("ModifyActivity.hbs", model)
                    })
                }
            })

            app.post("/User/:idActivity/ModifyActivity", function(request, response) {
                if (request.session.account == null || !request.session.account.id)
                {
                    return response.status(401).send()
                } else {
                    myDB.ModifyActivity(request.params.idActivity, request.body.name, request.body.starttime + " " + request.body.starthour, request.body.endtime + " " + request.body.endhour, request.body.description, function(status) {
                        if (status) {
                            response.status(200)
                            response.redirect("/")
                        }
                        else {
                            console.log("TG")
                        }
                    })
                }
            })

        //#endregion

    //#region FULLREST API

        //#region API User (GET, POST, PUT, DELETE)

            app.get("/api/User/", auth.ensureToken, function(request, response){
                myDB.SelectUsers(function (users){
                    response.status(200)
                    response.json({
                        users: users
                    })
                })
            })

            app.post("/api/User", auth.ensureToken, function(request, response){
                if (request.body.username == "" || request.body.username.includes(" ")) {
                    response.status(400)
                    response.json({
                        error: "Username can't be empty or contains space."
                    })
                }
                else {
                    myDB.CreateUser(request.body.username, request.body.password, request.body.date_of_birth, request.body.city, function (status) {
                        if (status) {
                            myDB.SelectUser(request.body.username, function(user) {
                                response.status(200)
                                response.json({
                                    user : user
                                })
        
                            })
                        }
                        else {
                            response.status(400)
                            response.json({
                                error: "Username already exist."
                            })
                        }
                    })
                }
            })

            app.put("/api/User/:id", auth.ensureToken, function(request, response) {
                myDB.ModifyUser(request.params.id, request.body.username, request.body.password, request.body.date_of_birth, request.body.city, function (status) {
                    if (request.body.username == "" || request.body.username.includes(" ")) {
                        response.status(400)
                        response.json({
                            error: "Username can't be empty or contain space."
                        })
                    }
                    if (!status) {
                        response.status(401)
                        response.json({
                            message : "Username already exist."
                        })
                    } else {
                        myDB.SelectUser(request.body.username, function(user) {
                            response.status(200)
                            response.json({
                                user: user
                            })
                        })
                    }
                })
            })

            app.delete("/api/User/:id", auth.ensureToken, function(request, response) {
                myDB.DeleteUser(request.params.id, function() {
                    response.status(200).send()
                })
            })

        //#endregion

        //#region API Weight (GET, POST, PUT, DELETE)

        app.get("/api/User/:id/Weights", auth.ensureToken, function(request, response) {
            myDB.SelectWeights(request.params.id, function (weights) {
                response.status(200)
                response.json({
                    weights: weights    
                })
            })
        })

        app.post("/api/User/:id/Weight", auth.ensureToken, function(request, response) {
            if (request.body.value < 20) {
                response.status(401)
                response.json({
                    error : "You can't add a weight inferior of 20 kg"
                })
            } else {
                myDB.CreateWeight(request.params.id, request.body.value, request.body.date, function (status, idWeight) {
                    if (status) {
                        myDB.SelectWeight(idWeight, function(weight) {
                            response.status(200).send()
                        })
                    } else {
                        response.status(400)    
                        response.json({
                            error: "There is a problem with the server, Try again."
                        })
                    }
                })
            }
        })

        app.post("/api/weights", auth.ensureToken, function(request, response) {
            if (request.body.value < 20) {
                response.status(401)
                response.json({
                    error : "You can't add a weight inferior of 20 kg"
                })
            } else {
                myDB.CreateWeight(request.body.userId, request.body.value, request.body.date, function (status, idWeight) {
                    if (status) {
                        myDB.SelectWeight(idWeight, function(weight) {
                            response.status(200).send()
                        })
                    } else {
                        response.status(400)    
                        response.json({
                            error: "There is a problem with the server, Try again."
                        })
                    }
                })
            }
        })

        app.put("/api/User/:id/Weight/:idWeight", auth.ensureToken, function(request, response) {
            myDB.ModifyWeight(request.params.idWeight, request.body.value, request.body.date, function(status) {
                if (status) {
                    myDB.SelectWeight(request.params.idWeight, function(weight) {
                        response.status(200)
                        response.json({
                            weight : weight
                        })
                    })
                } else {
                    response.status(400)
                    response.json({
                        error: "There is a problem with the server, Try again."
                    })
                }
            })
        })

        app.delete("/api/User/:id/Weight/:idWeight", auth.ensureToken, function(request, response) {
            myDB.DeleteWeight(request.params.idWeight, function(err) {
                if (err){
                    response.status(404)
                    return response.json({
                        error: "This weight doesn't exist."
                    })
                }
                response.status(200).send()
            })
        })

    //#endregion

        //#region API Activity (GET, POST, PUT, DELETE)

        app.get("/api/User/:id/Activities", auth.ensureToken, function(request, response){
                myDB.SelectActivities(request.params.id, function(activities) {
                    if (activities) {
                        response.status(200)
                        response.json({
                            activities: activities
                        })
                    } else {
                        response.status(401)
                        response.json({
                            error: "There is a problem with the server, Try again."
                        })
                    }
                })
        })

        app.post("/api/User/:id/Activity", auth.ensureToken, function(request, response){
            myDB.CreateActivity(request.params.id, request.body.name, request.body.starttime, request.body.endtime, request.body.description, function(status, idActivity) {
                if (status) {
                    myDB.SelectActivity(idActivity, function(activity) {
                        response.status(200).send()
                    })
                }
                else {
                    response.status(400)
                    response.json({
                        error: "There is a problem with the server, Try again"
                    })
                }
            })
        })

        app.post("/api/training-activities", auth.ensureToken, function(request, response){
            myDB.CreateActivity(request.body.userId, request.body.name, request.body.starttime, request.body.endtime, request.body.description, function(status, idActivity) {
                if (status) {
                    myDB.SelectActivity(idActivity, function(activity) {
                        response.status(200).send()
                    })
                }
                else {
                    response.status(400)
                    response.json({
                        error: "There is a problem with the server, Try again"
                    })
                }
            })
        })

        app.put("/api/User/:id/Activity/:idActivity", auth.ensureToken, function(request, response) {
            myDB.ModifyActivity(request.params.idActivity, request.body.name, request.body.starttime, request.body.endtime, request.body.description, function(status){
                if (status) {
                    myDB.SelectActivity(request.params.idActivity, function(activity) {
                        response.status(200)
                        response.json({
                            activity: activity
                        })
                    })
                }
                else {
                    response.status(404)
                    response.json({
                        error: "This activity doesn't exist."
                    })
                }
            })
        })

        app.delete("/api/User/:id/Activity/:idActivity", auth.ensureToken, function(request, response) {
            myDB.DeleteActivity(request.params.idActivity, function(status) {
                if (status) {
                    response.status(200).send()
                }
                else {
                    response.status(404)
                    response.json({
                        error: "This activity doesn't exist."
                    })
                }
            })
        })

    //#endregion

    //#endregion

//#endregion

app.post("/", function (request, response) {
   
})

app.listen(8080)