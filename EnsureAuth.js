let jwt = require('jsonwebtoken');

module.exports.ensureToken = function (request, response, next) {
    var bearerHeader = request.headers["authorization"];

    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        jwt.verify(bearerToken, 'secretaccesskey', (err, result) => {
            if (err) {
                response.status(401);
                response.json({
                    message: "invalid_grant"
                })
            }
            else {
                response.locals.apiUserId = result.userId;
                next();
            }
        });
    }
    else {
        response.status(401);
        response.json({
            message: "invalid_grant"
        })
    }
}