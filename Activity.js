function sqliteToJavascriptDate(startTime) {
    var date = startTime.split(" ")[0]
    var time = startTime.split(" ")[1]
    var year = date.split("-")[0]
    var month = date.split("-")[1]
    var day = date.split("-")[2]
    var hour = time.split(":")[0]
    var minutes = time.split(":")[1]
    return new Date(year, month, day, hour, minutes)
}

class Activity {
    
    constructor(id, name, startTime, endTime, description, user_id) {
        this.id = id
        this.name = name
        this.startDay = startTime.split(" ")[0]
        this.startHour = startTime.split(" ")[1]
        this.endDay = endTime.split(" ")[0]
        this.endHour = endTime.split(" ")[1]
        this.description = description
        this.user_id = user_id
    }
}


module.exports = Activity;