const sqlite = require('sqlite3').verbose()
const uuidv1 = require('uuid/v1')
const bcrypt = require('bcrypt')
const User = require("./User")
const Weight = require("./Weight")
const Activity = require("./Activity")


class MyDatabase { 
    constructor() {
        this.db = new sqlite.Database('./db/MYTD.db', sqlite.OPEN_READWRITE, (err) => {
            if (err) {
                return console.error(err.message);
            }
            console.log("Connected to the database.")
        });
    }

    destructor() {
        this.db.close((err) => {
            if (err) {
                console.error(err.message)
            }
            console.log('Close Database connection.')
        })
    }
    //#region CRUD User
    CreateUser(name, password, date_of_birth, city, callback) {
        let hash = bcrypt.hashSync(password, 10);
        this.db.run('INSERT INTO USER (ID, NAME, PASSWORD, DATE_OF_BIRTH, CITY) VALUES(?, ?, ?, ?, ?);', uuidv1(), name, hash, date_of_birth, city, function(err) {
            if (err) {
                callback(false)
                return
            }
            console.log(`Rows inserted: ${this.changes}`);
            callback(true)
        })
    }

    DeleteUser(id, callback) {
        let sql = 'DELETE FROM USER\
                    WHERE ID = "' + id + '"'
        this.db.run("DELETE FROM USER WHERE ID = ?", id, function(err) {
            if (err) {
                callback()
                return
            }
            console.log(`Row deleted: ${this.changes}`)
            callback()
        })
    }

    ModifyUser(id, name, password, date_of_birth, city, callback) {

        this.db.run("UPDATE USER SET NAME = ?, PASSWORD = ?, DATE_OF_BIRTH = ?, CITY = ? WHERE ID = ?", name, bcrypt.hashSync(password, 10), date_of_birth, city, id, function (err) {
            if (err) {
                console.log(err.message)
                callback(false)
                return
            }
            console.log(`Row updated: ${this.changes}`)
            callback(true)
        })
    }

    ModifyUserWithoutPassword(id, name, date_of_birth, city, callback) {
        this.db.run("UPDATE USER SET NAME = ?, DATE_OF_BIRTH = ?, CITY = ? WHERE ID = ?", name, date_of_birth, city, id, function (err) {
            if (err) {
                console.log(err.message)
                callback(false)
                return
            }
            console.log(`Row updated: ${this.changes}`)
            callback(true)
        })
    }

    CheckPasswordFromUser(username, password, callback) {
        this.db.all('SELECT PASSWORD as password FROM USER WHERE name = ?', username, (err, rows) => {
            if (err) {
                return console.error(err.message);
            }
            rows.forEach((row) => {
                if (bcrypt.compareSync(password, row.password)) {
                    callback(true)
                    return
                }
                else {
                    callback(false)
                    return
                }
            })
                if (rows.length <= 0) {
                    callback(false)
                }
        })
    }

    SelectUser(username, callback) {
        this.db.all("SELECT ID as id, NAME as username, PASSWORD as password, DATE_OF_BIRTH as date_of_birth, CITY as city FROM USER WHERE NAME = ?", username, (err, rows) => {
        if (err) {
            return console.error(err.message);
        }

        rows.forEach((row) => {
            callback(new User(row.id, row.username, row.password, row.date_of_birth, row.city))
        })
        });
    }

    SelectUserById(id, callback) {
        this.db.all("SELECT ID as id, NAME as username, PASSWORD as password, DATE_OF_BIRTH as date_of_birth, CITY as city FROM USER WHERE ID = ?", id, (err, rows) => {
        if (err) {
            return console.error(err.message);
        }

        rows.forEach((row) => {
            callback(new User(row.id, row.username, row.password, row.date_of_birth, row.city))
        })
        });
    }


    SelectUsers(callback) {
        this.db.all("SELECT ID as id, NAME as username, PASSWORD as password, DATE_OF_BIRTH as date_of_birth, CITY as city FROM USER", (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        var users = []
        rows.forEach((row) => {
            users.push(new User(row.id, row.username, row.password, row.date_of_birth, row.city))
        })
        callback(users)
        });
    }
    
    //#endregion

    //#region CRUD Weight
    CreateWeight(id, weight, date, callback) {

        let idWeight = uuidv1()
        this.db.run("INSERT INTO WEIGHT (ID, VALUE, DATE_W, USER_ID) VALUES(?, ?, ?, ?)", idWeight, weight, date, id, function(err) {
            if (err) {
                console.log(err)
                callback(false)
                return
            }
            console.log(`Rows inserted: ${this.changes}`);
            callback(true, idWeight)
        })
    }

    DeleteWeight(id, callback) {        
        this.db.run("DELETE FROM WEIGHT WHERE ID = ?", id, function(err) {
            if (err) {
                console.log(err.message)
                callback(err)
                return
            }
            console.log(`Row deleted: ${this.changes}`)
            callback()
        })
    }

    ModifyWeight(id, value, date, callback) {
            this.db.run("UPDATE WEIGHT SET VALUE = ?, DATE_W = ? WHERE ID = ?", value, date, id, function(err){
            if (err) {
                console.log(err.message)
                callback(false);
                return
            }
            callback(true)
            console.log(`Row modified: ${this.changes}`)
        })
    }
    
    SelectWeights(id, callback) {
        this.db.all("SELECT ID as id, VALUE as value, DATE_W as date, USER_ID as user_id FROM WEIGHT WHERE USER_ID = ? ORDER BY DATE_W", id, (err, rows) => {
            if (err) {
            return console.error(err.message);
            }
            var weights = []
            rows.forEach((row) => {
                weights.push(new Weight(row.id, row.value, row.date, row.user_id))
            })
            callback(weights)
        });
    }    
    
    SelectWeight(id, callback) {
        this.db.all("SELECT ID as id, VALUE as value, DATE_W as date, USER_ID as user_id FROM WEIGHT WHERE ID = ?", id, (err, rows) => {
            if (err) {
            return console.error(err.message);
            }
            var weight
            rows.forEach((row) => {
                weight = new Weight(row.id, row.value, row.date, row.user_id)
            })
            callback(weight)
        });
    }
    //#endregion
    
    //#region CRUD Activity
    CreateActivity(id, name, startTime, endTime, description, callback) {

        let idActivity = uuidv1()
        this.db.run("INSERT INTO ACTIVITY (ID, NAME, STARTTIME, ENDTIME, DESCRIPTION, USER_ID) VALUES(?, ?, ?, ?, ?, ?)", idActivity, name, startTime, endTime, description, id, function(err) {
            if (err) {
                console.log(err)
                callback(false)
                return
            }
            console.log(`Rows inserted: ${this.changes}`);
            callback(true, idActivity)
        })
    }

    DeleteActivity(id, callback) {
        this.db.run("DELETE FROM ACTIVITY WHERE ID = ?", id, function(err) {
            if (err || this.changes == 0) {
                console.log(err)
                callback(false)
                return
            }
            console.log(`Row deleted: ${this.changes}`)
            callback(true)
        })
    }

    ModifyActivity(id, name, startTime, endTime, description, callback) {
        this.db.run("UPDATE ACTIVITY SET NAME = ?, STARTTIME = ?, ENDTIME = ?, DESCRIPTION = ? WHERE ID = ?", name, startTime, endTime, description, id, function (err) {
        if (err || this.changes == 0) {
            console.log(err.message)
            callback(false)
            return
        }
        callback(true)
        console.log(`Row updated: ${this.changes}`)
        })
    }

    SelectActivities(id, callback) {
        this.db.all("SELECT ID as id, NAME as name, STARTTIME as starttime, ENDTIME as endtime, DESCRIPTION as description, USER_ID as user_id FROM ACTIVITY WHERE USER_ID = ? ORDER BY STARTTIME", id, (err, rows) => {
            if (err) {
                return console.error(err.message);
            }
            var activities = []
            rows.forEach((row) => {
                activities.push(new Activity(row.id, row.name, row.starttime, row.endtime, row.description, row.user_id))
            })
            callback(activities)
        });
    }
    
    SelectActivity(id, callback) {
        let sql = 

        this.db.all("SELECT ID as id, NAME as name, STARTTIME as starttime, ENDTIME as endtime, DESCRIPTION as description, USER_ID as user_id FROM ACTIVITY WHERE ID = ?", id, (err, rows) => {
            if (err) {
                return console.error(err.message);
            }
            var activity
            rows.forEach((row) => {
                activity = new Activity(row.id, row.name, row.starttime, row.endtime, row.description, row.user_id)
            })
            callback(activity)
        });
    }
    //#endregion
}

module.exports = MyDatabase